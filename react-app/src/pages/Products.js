import { Fragment, useState, useEffect, useContext } from 'react';
import { Row, Col,	 Container } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Products(){
	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);

	

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`).then(response => response.json()).then(data => {
			// console.log(data);
			setProducts(data.map(product => {
				// console.log(product);
				return(
					<ProductCard productProp = {product}/>
					)
			}))
		})
	}, [])

	if (user.id === null || user.id === undefined) {
	  return <Navigate to="/login" />;
	}

	if (user.isAdmin === true) {
	  return <Navigate to="/*" />;
	}

	return (
		<Fragment>
		        <Container className="my-3">
			        <h1 className="text-center">Our Products</h1>
			        <Row>
			          {products.map((product) => (
			            <Col key={product.props.productProp._id} sm={12} md={6} lg={4} xl={4}>
			                {product}
			            </Col>
			          ))}
			        </Row>
		        </Container>
		</Fragment>
	)
}