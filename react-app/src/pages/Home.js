import { Fragment } from 'react';
import { Row, Col } from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import PopiImage from '../images/Popi.jpg';

export default function Home(){
	return (
		<Fragment>
			<Banner />
			    <Row className="justify-content-center">
			        <Col xs={12} sm={8} md={6} lg={4} xl={3}>
			                <img
				                src={PopiImage}
				                alt="PopiOllie"
				                className="img-fluid"
				                style={{ maxHeight: '300px', maxWidth: '300px', objectFit: 'contain' }}
			                />
			        </Col>
			    </Row>
			<Highlights />
		</Fragment>
	)
}