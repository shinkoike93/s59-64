import { Link } from "react-router-dom";
 
export default function Error(){
    return (
        <div>
            <h1>Page Not Found</h1>
            <p>Go back to <Link to='/'>Home</Link> </p> 
        </div>
    )
}