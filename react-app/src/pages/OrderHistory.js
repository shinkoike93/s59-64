import React, { useEffect, useState, useContext } from 'react';
import { Table } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

const OrderHistory = () => {
	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);

	useEffect(() => {
	    fetchUserOrders();
	}, []);

	
	const fetchUserOrders = async () => {
		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			});
			const data = await response.json();
			setProducts(data);
		} catch (error) {
			console.error(error);
		}
	};

	if (!Array.isArray(products) || user.id === null || user.id === undefined || user.isAdmin === true) {
	    return <Navigate to="/*" />;
	}


	const renderOrderRows = () => {
	    return products.map((order) => (
	      	<tr key={order._id}>
		        <td>{order.productName}</td>
		        <td>{order.quantity}</td>
		        <td>{order.productId}</td>
	      	</tr>
	    ));
	};

	return (
		<div>
		    <h1 >Order History</h1>
		    <Table striped bordered responsive>
		        <thead className="bg-dark text-light">
		          	<tr>
			            <th>Product Name</th>
			            <th>Quantity</th>
			            <th>Product ID</th>
		          	</tr>
		        </thead>
		        <tbody>
		          	{renderOrderRows()}
		        </tbody>
		    </Table>
	    </div>
	);
};

export default OrderHistory;