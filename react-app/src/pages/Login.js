import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal2 from 'sweetalert2'
import UserContext from '../UserContext';

export default function Login() {

	const navigate = useNavigate();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);
	const { user, setUser } = useContext(UserContext);


	function authenticate(e) {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(response => response.json()).then(data => {
			console.log(data);
			if(data === false){
				Swal2.fire({
					title: "Login unsuccessful!",
					icon:'error',
					text: 'Check your login credentials and try again'
				})

			}else{
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);
				Swal2.fire({
					title: "Login successful!",
					icon:'success',
					text: 'Welcome to PopiOllie!'
				})
				navigate('/')
			}
		})
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		}).then(response => response.json()).then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !== '' && password !== '' && email.length<=25){
			setIsActive(false);
		}else{
			setIsActive(true);
		}

	}, [email, password]);

	return (
		user.id === null || user.id === undefined
		?
		<Row>
			<Col className = 'col-6 mx-auto'>
			<h1 className = 'text-center mt-2'>Login</h1>
			<Form onSubmit={(e) => authenticate(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>Email address</Form.Label>
					<Form.Control 
					type="email" 
					placeholder="Enter email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
					/>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control 
					type="password" 
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
					/>
				</Form.Group>
				<Button variant="primary" type="submit" id="submitBtn" disabled = {isActive} className ='mt-3'>
					Login
				</Button>
			</Form>
		</Col>
	</Row>
	:
	<Navigate to = '/*' /> 
	)
}