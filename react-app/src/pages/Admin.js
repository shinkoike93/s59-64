import React, { Fragment, useState, useEffect, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import AdminDashboard from '../components/AdminDashboard';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function AdminPage() {
	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);
	useEffect(() => {
		if (user.isAdmin === true) {
			fetch(`${process.env.REACT_APP_API_URL}/products/`)
			.then((response) => response.json())
			.then((data) => {
				setProducts(
				data.map((product) => (
				<ProductCard key={product.id} productProp={product} />
				))
				);
			});
		}
	}, [user]);

	return (
	<Fragment>
		{user.id !== null || user.id !== undefined || user.isAdmin === true ? (
		<Fragment>
			<AdminDashboard />
			<hr />
		</Fragment>
		) : (
		<Navigate to = '/*'/>
		)}
	</Fragment>
	);
}