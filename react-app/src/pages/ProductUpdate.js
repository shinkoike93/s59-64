import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

const ProductUpdate = () => {
	const navigate = useNavigate();
	const { id } = useParams();
	const { user } = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [image, setImage] = useState('');

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then((response) => response.json())
		.then((data) => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
		.catch((error) => {
			console.error('Error fetching product:', error);
		});
	}, [id]);

	if (user.id === null || user.id === undefined || user.isAdmin === false) {
	    return <Navigate to="/*" />;
	}

	const Update = (e) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/update`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				image: image
			})
		})
		.then((response) => response.json())
		.then((data) => {
			console.log(data);
			if (data !== false) {
				Swal.fire({
					title: 'Successfully updated the product!',
					icon: 'success',
					text: 'Good job!',
				}).then(() => {
					navigate('/admin');
				});
			} else {
				Swal.fire({
					title: 'Failed to update the product!',
					icon: 'error',
					text: 'Oh no!',
				});
			}
		})
	};

	return (
		user.id !== null || user.id !== undefined || user.isAdmin !== false
		?
		<Row>
			<Col className="col-6 mx-auto">
			<h1>Update Product</h1>
			<Form onSubmit={Update}>
				<Form.Group className="mb-3" controlId="name">
					<Form.Label>Name</Form.Label>
					<Form.Control
					type="text"
					placeholder="Enter Product Name"
					value={name}
					onChange={(e) => setName(e.target.value)}
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="description">
					<Form.Label>Description</Form.Label>
					<Form.Control
					as="textarea"
					rows={3}
					placeholder="Enter Product Description"
					value={description}
					onChange={(e) => setDescription(e.target.value)}
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="price">
					<Form.Label>Price</Form.Label>
					<Form.Control
					type="text"
					placeholder="Enter Product Price"
					value={price}
					onChange={(e) => setPrice(e.target.value)}
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="image">
					<Form.Label>Image</Form.Label>
					<Form.Control
					type="text"
					placeholder="Enter Image Link"
					value={image}
					onChange={(e) => setImage(e.target.value)}
					/>
				</Form.Group>

				<Button  variant="primary" type="submit">
					Update
				</Button>
				<Button className="mx-1" variant="secondary" onClick={() => navigate('/Admin')}>
					Cancel
				</Button>
			</Form>
		</Col>
	</Row>
	:
	<Navigate to = '/*' /> 
	);
};

export default ProductUpdate;