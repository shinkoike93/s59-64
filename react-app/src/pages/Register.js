import {Button, Form, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal2 from 'sweetalert2';

export default function Register(){

const navigate = useNavigate();
const { user } = useContext(UserContext);

//State hooks to store the values of the input fields
const [email, setEmail] = useState('');
const [firstname, setFirstName] = useState('');
const [lastname, setLastName] = useState('');
const [mobilenumber, setMobileNumber] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');
const [isPassed, setIsPassed] = useState(true);
const [isEnough, setIsEnough] = useState(true);
const [isDisabled, setIsDisabled] = useState(true);
const [isPasswordMatch, setIsPasswordMatch] = useState(true); 
 
useEffect(()=>{
	if(email.length > 25){
		setIsPassed(false);
	}else{
		setIsPassed(true);
	}
}, [email]);

useEffect(()=>{
	if(mobilenumber.length > 11){
		setIsEnough(false);
	}else{
		setIsEnough(true);
	}
}, [mobilenumber]);

//this useEffect will disable or enable our sign up button
useEffect(()=> {
	if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && email.length <= 25 && firstname !== '' && lastname !== '' && mobilenumber !== '' && mobilenumber.length <= 11){
		setIsDisabled(false);
	}else{
		setIsDisabled(true);
	}
}, [email, password1, password2, firstname, lastname, mobilenumber]);

//function to simulate user registration
function registerUser(event) {
	//prevent page reloading
	event.preventDefault();

	fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
		   method: 'POST',
		   headers: {
		       'Content-Type' : 'application/json'
		   },
		   body: JSON.stringify({
		       firstName: firstname,
		       lastName: lastname,
		       email: email,
		       mobileNo: mobilenumber,
		       password: password1,
		   })
	}).then(response => response.json()).then(data => {
		   console.log(data);
		   if(data === false){
		       Swal2.fire({
		           title: "Registration unsuccessful!",
		           icon:'error',
		           text: 'The email address is already registered. Please use another email address to register.'
		       })

		   }else{
		    	setFirstName('');
		    	setLastName('');
		       	setEmail('');
		       	setMobileNumber('');
		       	setPassword1('');
		       	setPassword2('');

		       Swal2.fire({
		           title: "Registration successful!",
		           icon:'success',
		           text: 'Welcome to PopiOllie!'
		       })
		       navigate('/login')
		   }
	})
}

	//useEffect to validate whether the password1 is equal to password2
	useEffect(() => {
		if(password1 !== password2){
			setIsPasswordMatch(false);
		}else{
			setIsPasswordMatch(true);
		}

	}, [password1, password2]);

	return(
		user.id === null || user.id === undefined
       	?
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Register</h1>
				<Form onSubmit ={event => registerUser(event)}>
					<Form.Group className="mb-3" controlId="formBasicText1">
				    	<Form.Label>First Name</Form.Label>
				    	<Form.Control 
				        	type="text" 
				        	placeholder="Enter first name"
				        	value = {firstname}
				        	onChange = {event => setFirstName(event.target.value)}
				        	/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="formBasicText2">
				    	<Form.Label>Last Name</Form.Label>
				    	<Form.Control 
				        	type="text" 
				        	placeholder="Enter last name"
				        	value = {lastname}
				        	onChange = {event => setLastName(event.target.value)}
				        	/>
					</Form.Group>

	
				    <Form.Group className="mb-3" controlId="formBasicEmail">
				    	<Form.Label>Email address</Form.Label>
				     	<Form.Control 
				        	type="email" 
				        	placeholder="Enter email" 
				        	value = {email}
				        	onChange = {event => setEmail(event.target.value)}
				        	/	>
				    	<Form.Text className="text-danger" hidden = {isPassed}>
				        	The email should not exceed 25 characters!
				    	</Form.Text>
					</Form.Group>

					<Form.Group className="mb-3" controlId="formBasicNumber">
				    	<Form.Label>Mobile Number</Form.Label>
				    	<Form.Control 
				        	type="number" 
				        	placeholder="Enter Mobile Number"
				        	value = {mobilenumber}
				        	onChange = {event => setMobileNumber(event.target.value)}
				    	/>
				   		<Form.Text className="text-danger" hidden = {isEnough}>
				        	Number length must be 11 digits.
				    	</Form.Text>
					</Form.Group>

					<Form.Group className="mb-3" controlId="formBasicPassword1">
				    	<Form.Label>Password</Form.Label>
				    	<Form.Control 
				       	 	type="password" 
				        	placeholder="Password" 
				        	value = {password1}
				        	onChange = {event => setPassword1(event.target.value)}
				    	/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="formBasicPassword2">
				    	<Form.Label>Verify Password</Form.Label>
				    	<Form.Control 
				        	type="password" 
				        	placeholder="Verify Password" 
				        	value = {password2}
				        	onChange = {event => setPassword2(event.target.value)}
				    	/>

				    	<Form.Text className="text-danger" hidden = {isPasswordMatch}>
				        	The passwords does not match!
				    	</Form.Text>
				    	</Form.Group>

				    	<Button variant="danger" type="submit" disabled = {isDisabled}>
				       		Submit
				    	</Button>
				</Form>
			</Col>
		</Row>
		:
    	<Navigate to = '/*' />
	)
}