import { Button, Card, Container, Row, Col } from 'react-bootstrap';
import { useContext } from 'react';
import { Link, Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductCard({productProp}){

	const {_id, name, description, price, image} = productProp;
	const { user } = useContext(UserContext);

	if (user.id === null || user.id === undefined) {
	  return <Navigate to="/login" />;
	}

	if (user.isAdmin === true) {
	  return <Navigate to="/*" />;
	}
	
	return (
		<Container className="my-3">
		    <Row className="justify-content-center">
		      	<Col>
		        	<Card className="mb-3">
			           <Card.Img src={image} className="w-200 h-200" style={{ objectFit: 'cover' }} />
			           <Card.Body>
				            <Card.Title>{name}</Card.Title>
				            <Card.Subtitle>Description:</Card.Subtitle>
				            <Card.Text>{description}</Card.Text>
				            <Card.Subtitle>Price:</Card.Subtitle>
				            <Card.Text>{price}</Card.Text>
				            <Button as={Link} to={`/products/${_id}`} variant="primary">
				              Details
				            </Button>
			           </Card.Body>
		        	</Card>
		      	</Col>
		    </Row>
		</Container>
	);
};

