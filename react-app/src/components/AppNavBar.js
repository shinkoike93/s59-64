import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { useContext } from 'react';
import {Link} from 'react-router-dom';

import UserContext from '../UserContext';

// Navbar of the pages
export default function AppNavBar() {

	const { user } = useContext(UserContext);
	
	return (
		<Navbar bg="dark" variant="dark" expand="lg" sticky="top">
		    <Container>
		        <Navbar.Brand as={Link} to="/">
		          	PopiOllie
		        </Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          	<Nav className="ml-auto">
			            <Nav.Link as={Link} to="/" exact="true">
			              	Home
			            </Nav.Link>
			            {user.id !== null && user.id !== undefined && user.isAdmin === false && (
				            <>
					            <Nav.Link as={Link} to="/products">
					              	Products
					            </Nav.Link>
					              <Nav.Link as={Link} to="/orderhistory">
					              	Order History
					            </Nav.Link>
				            </>

			            )}
			            {user.id === null || user.id === undefined ? (
			              	<>
			                	<Nav.Link as={Link} to="/register">
			                  		Register
			                	</Nav.Link>
			                	<Nav.Link as={Link} to="/login">
			                  		Login
			                	</Nav.Link>
			              	</>
			            ) : (
			              	<>
				                {user.id !== null && user.id !== undefined && user.isAdmin === true && (
				                  <Nav.Link as={Link} to="/admin">
				                    Admin
				                  </Nav.Link>
				                )}
				                <Nav.Link as={Link} to="/logout">
				                  	Logout
				                </Nav.Link>
			              	</>
			            )}
		          </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
}