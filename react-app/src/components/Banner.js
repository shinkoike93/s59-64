import { Button, Row, Col } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(){
	return (
		<Row>
		    <Col className="p-5">
		        <h1>PopiOllie's goods</h1>
		        <p>Get your order now</p>
		        <Button as = {Link} to = '/products' variant="primary">Check Products!</Button>
		    </Col>
		</Row>
	)
}