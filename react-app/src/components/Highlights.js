import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row className="mt-3 mb-3">
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Order from Home</h2>
		                </Card.Title>
		                <Card.Text>
		                    Ordering from home offers convenience, saves time, and provides the flexibility to order anytime, day or night. It allows our customers to explore menus and products in detail, personalize their orders, and enjoy contactless and hygienic transactions. With delivery and pickup options, our customers can enjoy their favorite items without leaving their homes.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Online payment</h2>
		                </Card.Title>
		                <Card.Text>
		                    Online payment provides the ultimate convenience and security, giving you the power to shop from anywhere at any time. Say goodbye to the hassles of carrying cash or visiting physical stores – with online payment, you have the freedom to make purchases from the comfort of your own home or while on the go.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Be a member</h2>
		                </Card.Title>
		                <Card.Text>
		                    By becoming a member, you gain exclusive access to a world of benefits and stay at the forefront of our product updates. As a valued member, you will be the first to know about our latest releases, exciting promotions, and special offers.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
	 	</Row>
	)
}