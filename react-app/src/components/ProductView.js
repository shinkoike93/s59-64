import {Row, Col, Button, Card} from 'react-bootstrap';
import {useParams, useNavigate, Navigate} from 'react-router-dom';
import {useEffect, useState, useContext} from	'react';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';
import InputSpinner from 'react-bootstrap-input-spinner';

export default function ProductView(){

	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');
	const [totalAmount, setTotalAmount] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const { user } = useContext(UserContext);

	const navigate = useNavigate();
	const {id} = useParams();
	// console.log(user.id);

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`).then(response => response.json()).then(data => {
				setName(data.name);
				setPrice(data.price);
				setDescription(data.description);
	       })
	}, [id])

	useEffect(() => {
	    const calculatedTotal = price * quantity;
	    setTotalAmount(calculatedTotal);
	}, [price, quantity]);
		
	if (user.id === null || user.id === undefined || user.isAdmin === true) {
	    return <Navigate to="/*" />;
	}

	const QuantityChange = (value) => {

	  setQuantity(value);
	};


	const order = (productId) => {
			console.log(totalAmount)
			fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				productId: `${productId}`,
				productName: name,
				quantity: quantity,
			})
		}).then(response => response.json()).then(data => {
			// console.log(data);
			setQuantity('');
			setTotalAmount('');

			if (data !== false){
				// successful login
				Swal2.fire({
					title: "Order successful!",
					icon:'success',
					text: 'We are now preparing your order!'
				}).then(() => {
					navigate('/products');
				});
			}else{
				Swal2.fire({
					title: "Order unsuccessful!",
					icon:'error',
					text: '{Please try again!}'
				})
			}
				
		})
	}

	return(
		<Row>
			<Col>
				<h1 className = "text-center">Order Product</h1>
				<Card>
					<Card.Body>
						   <Card.Title>{name}</Card.Title>
						   <Card.Text>
						     {description}
						   </Card.Text>
						   <Card.Text>
						     Price: {price}
						   </Card.Text>
							<Row className="mb-3">
							    <Col xs={6} sm={4} md={3} lg={2}>
							        <InputSpinner
							            type="real"
							            precision={2}
							            max={20}
							            min={1}
							            step={1}
							            value={quantity}
							            onChange={QuantityChange}
							            variant="dark"
							            size="sm"
							        />
							    </Col>
							</Row>
						   <Card.Text>Total Amount: {totalAmount}</Card.Text>
						   <Button variant="primary" onClick = {() => order(id)}>Checkout</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}