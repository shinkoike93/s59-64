import React, { useState, useEffect, useContext } from 'react';
import { Button, Table } from 'react-bootstrap';
import { Link, useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

const AdminDashboard = () => {
	const navigate = useNavigate();
	const [products, setProducts] = useState([]);

	const { user } = useContext(UserContext);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then((response) => response.json())
		.then((data) => {
			setProducts(data);
		});
	}, []);

	if (user.id === null || user.id === undefined || user.isAdmin === false) {
	    return <Navigate to="/*" />;
	}

	const Deactivate = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
		.then((response) => response.json())
		.then((data) => {
			console.log(data);
			Swal.fire({
				title: 'Successfully Created a product!',
				icon: 'success',
				text: 'Good job!',
			})
			navigate('/admin')
		})
	};

	const Reactivate = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
		.then((response) => response.json())
		.then((data) => {
			console.log(data);
			Swal.fire({
				title: 'Successfully Created a product!',
				icon: 'success',
				text: 'Good job!',
			})
			navigate('/admin')
		})
	};

	const showProductCard = (product) => {
		return (
			<tr key={product._id}>
				<td style={{ width: '100px', height: '100px', padding: '0' }}>
				   	<img src={product.image} alt="Product" className="w-100 h-100" style={{ objectFit: 'cover' }} />
				</td>
				<td>{product.name}</td>
				<td>{product.description}</td>
				<td>{product.price}</td>
				<td>{product.isActive ? 'Active' : 'Inactive'}</td>
				<td>
					<Button variant="danger" onClick={() => Deactivate(product._id)}>
						Deactivate
					</Button>
					<Button variant="success" onClick={() => Reactivate(product._id)}>
						Reactivate
					</Button>
					<Link to={`/ProductUpdate/${product._id}`}>
					<Button variant="primary">
						Update
					</Button>
				</Link>
			</td>
		</tr>
		);
	};

return (
		<div>
			<h1>Admin Dashboard</h1>
			<h3>Product List</h3>
			<Link to="/CreateProduct">
			<Button variant="primary">Create Product</Button>
		</Link>
		<hr />
		<div className="table-responsive">
		<Table striped bordered responsive>
			<thead className="bg-dark text-light">
				<tr>
					<th>Image</th>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				{products.map((product) => showProductCard(product))}
			</tbody>
		</Table>
		</div>
		</div>
	);
};

export default AdminDashboard;