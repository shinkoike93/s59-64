import { Container } from 'react-bootstrap';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Products from './pages/Products';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Admin from './pages/Admin';
import CreateProduct from './pages/CreateProduct';
import ProductUpdate from './pages/ProductUpdate';
import OrderHistory from './pages/OrderHistory';

import {useState, useEffect} from 'react';
import { UserProvider } from './UserContext';

import './App.css';

import ProductView from './components/ProductView';

function App() {
	const [user, setUser] = useState({
		id: null,
		isAdmin: null
	});

	// Function is for clearing localStorage on logout
	const unsetUser = () => {
		localStorage.clear()
	};

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}, [])

	return (
	<UserProvider value = {{user, setUser, unsetUser}}>
		<BrowserRouter>
			<AppNavBar />
			<Container>
				<Routes>
					<Route exact path = '/' element = {<Home/>}/>
					<Route path = '/products' element = {<Products/>}/>
					<Route path = '/register' element = {<Register/>}/>
					<Route path = '/login' element = {<Login/>}/>
					<Route path = '/logout' element = {<Logout/>}/>
					<Route path = '/products/:id' element = {<ProductView/>}/>
					<Route path = '/admin' element = {<Admin/>}/>
					<Route path = '/createproduct' element = {<CreateProduct/>}/>
					<Route path = '/orderhistory' element = {<OrderHistory/>}/>
					<Route path = '/productupdate/:id' element = {<ProductUpdate/>}/>
					{/*FallBack route*/}
					<Route path="*" element={<Error/>} />
				</Routes>
			</Container>
		</BrowserRouter>
	</UserProvider>
	);
}

export default App;